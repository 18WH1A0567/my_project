l = [int(x) for x in input().split('+')]
l.sort()
n = len(l)
for i in range(n-1):
    print(l[i], end='')
    print('+', end='')
print(l[n-1])