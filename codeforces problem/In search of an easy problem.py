t = int(input())
l = [int(x) for x in input().split()]
s = set(l)
if 1 in s:
    print('HARD')
else:
    print('EASY')