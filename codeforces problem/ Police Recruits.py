n = int(input())
l = list(map(int, input().split()))
p = 0
c = l.count(-1)
for i in l:
    if i > 0:
        p += i
    else:
        if p > 0:
            p -= 1
            c -= 1
print(c)