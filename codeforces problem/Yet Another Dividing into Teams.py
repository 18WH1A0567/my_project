t = int(input())
for q in range(t):
    n = int(input())
    l = [int(x) for x in input().split()]
    l.sort()
    c = 1
    for i in range(n-1):
        if l[i+1] - l[i] > 1:
            continue
        else:
            c += 1
            break
    print(c)
 