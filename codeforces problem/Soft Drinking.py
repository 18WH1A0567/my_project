n, k, l, c, d, p, nl, np = [int(x) for x in input().split()]
tot_lit = (k * l) // nl
tot_slice = c * d
tot_salt = p // np
#print('litres ', tot_lit, 'tot_slice ', tot_slice , 'tot_salt ', tot_salt)
ans = min(tot_lit, tot_slice, tot_salt)
print(ans//n)