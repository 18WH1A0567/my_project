l, d = [int(x) for x in input().split()]
f = True
for i in range(pow(10, l-1), pow(10, l)):
    if i % d == 0:
        print(i)
        f = False
        break
if f:
    print('-1')