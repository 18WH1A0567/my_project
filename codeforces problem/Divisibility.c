#include<stdio.h>
 
int main(){
        int t, a, b, r;
 
        scanf("%d", &t);
 
        for(int i = 0;i < t; i++){
                scanf("%d%d", &a, &b);
                if ( a % b == 0){
                        printf("0\n");
                }
                else{
                        r = a % b;
                        printf("%d\n", b-r);
                }
 
        }
        return 0;
}
