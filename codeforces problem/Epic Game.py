import math
a, b, n = [int(x) for x in input().split()]
i = 1
while(True):
    if i % 2 == 1:
        x = math.gcd(a, n)
        if x > n:
            print('1')
            break
        n -= x
    else:
        x = math.gcd(b, n)
        if x > n:
            print('0')
            break
        n -= x
    i += 1