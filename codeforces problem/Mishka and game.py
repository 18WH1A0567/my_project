t = int(input())
m = 0
c = 0
for q in range(t):
    x, y = [int(x) for x in input().split()]
    if x > y:
        m += 1
    elif x < y:
        c += 1
if m > c:
    print('Mishka')
elif m < c:
    print('Chris')
else:
    print('Friendship is magic!^^')