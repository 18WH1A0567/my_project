n, m = map(int, input().split())
f = True
for q in range(n):
    l = list(map(str, input().split()))
    if f :
        for i in l:
            if i not in 'BWG':
                f = False
                break
if f:
    print('#Black&White')
else:
    print('#Color')