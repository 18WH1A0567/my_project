def isLucky(s):
    s = str(s)
    for i in s:
        #print(i)
        if i == '4':
            continue
        if i == '7':
            continue
        else:
            return False
    
    return True
 
n = input()
s = set(int(i) for i in n)
if isLucky(s):
    print('YES')
else:
    n = int(n)
    i = 4
    while(i <= n ):
        if isLucky(i):
            #print(i)
            if n % i == 0:
                print('YES')
                break
        i += 1
    if i > n:
        print('NO')