m = input()
string = input()
 
f = 'qwertyuiop'
s = 'asdfghjkl;'
t = 'zxcvbnm,./'
 
def search_(i):
    if i in f:
        return 'f'
    elif i in s:
        return 's'
    return 't'
 
if m == 'R':
    for i in string:
        if search_(i) == 'f':
            print(f[f.index(i) - 1], end = '')
        elif search_(i) == 's':
            print(s[s.index(i) - 1], end = '')
        else:
            print(t[t.index(i) - 1], end = '')
else:
    for i in string:
        if search_(i) == 'f':
            print(f[f.index(i) + 1], end = '')
        elif search_(i) == 's':
            print(s[s.index(i) + 1], end = '')
        else:
            print(t[t.index(i) + 1], end = '')
print()