def sum1(n):
    s = 0
    for i in range(1, n+1):
        s += i
    return s
 
n = int(input())
t = 1
l = 0
while(True):
    if l < n:
        #print(l)
        t += 1
        l += sum1(t)
    else:
        print(t-1)
        break