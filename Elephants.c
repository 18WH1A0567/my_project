#include<stdio.h>
 
int main(){
        int n, c = 0;
        scanf("%d", &n);
        while(n > 0){
                if( n >= 5){
                        c++;
                        n -= 5;
                }
                else{
                        c++;
                        n = 0;
                }
        }
        printf("%d\n", c);
        return 0;
}