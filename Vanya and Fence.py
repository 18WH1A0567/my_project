n, h = [int(x) for x in input().split()]
l = [int(x) for x in input().split()]
s = 0
for i in l:
    if i <= h:
        s += 1
    else:
        s += 2
print(s)