n, k = [int(x) for x in input().split()]
while(k > 0):
    r = n % 10
    if r == 0:
        n = int(n/10)
    else:
        n -= 1
    k -= 1
print(n)