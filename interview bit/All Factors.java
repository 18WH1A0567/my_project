public class Solution {
    public ArrayList<Integer> allFactors(int A) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for(int i = 1; i <= Math.sqrt(A); i++){
            if(A % i == 0){
                list.add(i);
                if(A/i != Math.sqrt(A)){
                    list.add(A/i);
                }
            }
        }
        Collections.sort(list);
        return list;
    }
}
