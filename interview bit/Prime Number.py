public class Solution {
    
    public boolean isPrime(int n){
        if(n == 2){
            return true;
        }
        for(int i = 2; i <= Math.sqrt(n); i++){
            if (n % i == 0){
                return false;
            }
        }
        return true;
    }
    
    public ArrayList<Integer> sieve(int A) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for(int i = 2; i <= A; i++){
            if(isPrime(i)){
                list.add(i);
            }
        }
        return list;
        
    }
}
