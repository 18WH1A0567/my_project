class Solution:
    # @param A : integer
    # @param B : integer
    # @return an integer
    
    def cpFact(self, A, B):
        k = B
        for i in reversed(range(A+1)):
            if A % i == 0:
                j = i
                while(i != B):
                    if i > B:
                        i -= B
                    else:
                        B -= i
                if i == 1:
                    return j
                B = k
        return 0