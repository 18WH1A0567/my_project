public class Solution {
    public boolean is_Prime(int n){
        if(n == 2){
            return true;
        }
        if(n == 1){
            return false;
        }
        for(int i = 2; i <= Math.sqrt(n); i++){
            if(n % i == 0){
                return false;
            }
        }
        return true;
    }
    public int isPrime(int A) {
        if(is_Prime(A)){
            return 1;
        }
        return 0;
    }
}
