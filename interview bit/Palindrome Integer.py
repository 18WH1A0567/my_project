class Solution:
    # @param A : integer
    # @return an integer
    def isPalindrome(self, A):
        s = str(A)
        l = len(s)
        f = True
        for i in range(l):
            if s[i] != s[l-i-1]:
                f = False
                break
        if f:
            return 1
        return 0
