# cook your dish here
t = int(input())
for q in range(t):
    x, y = [int(x) for x in input().split()]
    if x - y <= 0:
        print('0')
    else:
        print(x - y)