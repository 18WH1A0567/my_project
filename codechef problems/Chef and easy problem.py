# cook your dish here
t = int(input())
for q in range(t):
    n = int(input())
    l = [int(x) for x in input().split()]
    c = 0
    r = 0
    l.sort(reverse = True)
    for i in range(n):
        if i % 2 == 0:
            c += l[i]
        else:
            r += l[i]
    if r > c:
        print(r)
    else:
        print(c)