#include <stdio.h>

int main(void) {

	int t, i = 1, x;
	scanf("%d", &t);
	while(i <= t){
	    scanf("%d", &x);
	    if( x < 10){
	        printf("What an obedient servant you are!\n");
	    }
	    else{
	        printf("-1\n");
	    }
	    i++;
	    
	}
	
	return 0;
}