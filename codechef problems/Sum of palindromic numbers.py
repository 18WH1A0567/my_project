def isPalindrome(n):
    s = str(n)
    if s == s[::-1]:
        return True
    return False

t = int(input()) 
for q in range(t):
    l, r = map(int, input().split())
    c = 0
    for i in range(l, r+1):
        if isPalindrome(i):
            c += i
    print(c)

