#include<stdio.h>

void check(int h, float c, int t){
        if(h > 50 && c < 0.7 && t > 5600)
                printf("10\n");
        else if(h > 50 && c < 0.7)
                printf("9\n");
        else if(c < 0.7 && t > 5600)
                printf("8\n");
        else if(h > 50 && t > 5600)
                printf("7\n");
        else if(h > 50 || c < 0.7 || t > 5600)
                printf("6\n");
        else
                printf("5\n");
}

int main(){
        int n, h, t;
        float c;
        scanf("%d", &n);
        for(int i = 0; i < n; i++){
                scanf("%d%f%d", &h, &c, &t);
                check(h, c, t);
        }
        return 0;
}