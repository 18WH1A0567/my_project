/* package codechef; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class Codechef
{
	public static void main (String[] args) throws java.lang.Exception
	{
		// your code goes here
		int t, n, m, d = 0;
		Scanner scan = new Scanner(System.in);
		 
		t = scan.nextInt();
		for(int q = 0; q < t; q++){
		    n = scan.nextInt();
		    m = scan.nextInt();
		    d = 0;
		    
		    ArrayList<Integer> list = new ArrayList<Integer>();
		    
		    for(int i = 0;i < n; i++){
		        list.add(scan.nextInt());
		    }
		    
		    int l = Collections.max(list);
		    
		    for(Integer i: list){
		        d += (l - i);
		    }
		    
		    if(d == m){
		        System.out.println("Yes");
		    }
		    else{
		        System.out.println("No");
		    }
		    
		    
		}
	}
}
