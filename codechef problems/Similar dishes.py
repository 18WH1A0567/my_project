# cook your dish here
t = int(input())
for q in range(t):
    s1 = [str(x) for x in input().split()]
    s2 = [str(x) for x in input().split()]
    c = 0
    for i in s1:
        if i in s2:
            c += 1
    if c >= 2:
        print('similar')
    else:
        print('dissimilar')