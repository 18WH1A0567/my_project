t = int(input())
for i in range(t):
    l = [int(x) for x in input().split()]
    s = sum(l)
    if(s == 180):
        print("YES")
    else:
        print("NO")