# cook your dish here
t = int(input())
for i in range(t):
    n = int(input())
    s = input()
    l = set(str(i) for i in s)
    if 'I' in l:
        print('INDIAN')
    elif 'Y' in l:
        print('NOT INDIAN')
    else:
        print('NOT SURE')