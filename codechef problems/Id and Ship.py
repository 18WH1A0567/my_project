n = int(input())
for i in range(n):
    ch = input().lower()
    if ch == 'b':
        print('BattleShip')
    elif ch == 'c':
        print('Cruiser')
    elif ch == 'd':
        print('Destroyer')
    else:
        print('Frigate')