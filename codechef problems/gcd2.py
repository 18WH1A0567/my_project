# cook your dish here
import math

t = int(input())
for q in range(t):
    a,b = [int(x) for x in input().split()]
    print(math.gcd(a, b))
    