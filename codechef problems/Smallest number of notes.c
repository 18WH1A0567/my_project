#include <stdio.h>

int main(void) {

	int n, x, c;
	scanf("%d", &x);
	for(int i = 1; i <= x; i++){
	    scanf("%d", &n);
	    c = 0;
	    c = n / 100;
	    n %= 100;
	    c += (n / 50);
	    n %= 50;
	    c += (n / 10);
	    n %= 10;
	    c += (n / 5);
	    n %= 5;
	    c += (n / 2);
	    n %= 2;
	    c += n;
	    printf("%d\n", c);
	}
	return 0;
}