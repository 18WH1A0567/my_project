t = int(input())
for q in range(t):
    n, m = map(int, input().split())
    l = [int(x) for x in input().split()]
    r = []
    for i in range(1, n+1):
        if i not in l:
            r.append(i)
    r_l = len(r)
    c = []
    a = []
    for i in range(r_l):
        if i % 2 == 0:
            c.append(r[i])
        else:
            a.append(r[i])
    for i in c:
        print(i, end= ' ')
    print()
    for i in a:
        print(i, end= ' ')
    print()
