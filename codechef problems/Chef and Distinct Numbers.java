/* package codechef; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class Codechef
{
	public static void main (String[] args) throws java.lang.Exception
	{
		// your code goes here
	Scanner scan = new Scanner(System.in);
		int t = scan.nextInt();
		
		for(int i = 1; i <= t; i++){
		    
		    int n = scan.nextInt();
		    int[] a = new int[n];
		    boolean flag = true;
		    
		    for(int k = 0; k < n; k++){
		    	a[k] = scan.nextInt();
		    }
		    
		    for(int k = 0; k< n-1; k++){
		        for(int j = k+1; j < n; j++){
		            if(a[k] == a[j] ){
		                System.out.println("Yes");
		                flag = false;
		                break;
		            }
		        }
		    }
		    if(flag){
		        System.out.println("No");
		    }
	
	}
}
