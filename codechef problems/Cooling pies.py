t = int(input())
for i in range(t):
    n = int(input())
    p = list(map(int, input().split()))
    r = list(map(int, input().split()))
    p.sort()
    r.sort()
    c = 0
    for i in p:
        for j in r:
            if i <= j:
                c += 1
                r.remove(j)
                break
    print(c)

    
