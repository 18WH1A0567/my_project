#include <stdio.h>

int main(void) {
	// your code goes here
	int t, n;
	scanf("%d", &t);
	for(int i = 1; i <= t; i++){
	    scanf("%d", &n);
	    int a[n], s = 0;
	    for(int j = 0;j < n; j++){
	        scanf("%d", &a[j]);
	        s += a[j];
	    }
	    if(s % 2 == 0){
	        printf("NO\n");
	    }
	    else{
	        printf("YES\n");
	    }
	}
	return 0;
}

