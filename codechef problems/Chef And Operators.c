#include <stdio.h>

int main(void) {

	int n, x , y;
	scanf("%d", &n);
	for(int i = 1; i <= n; i++){
	    scanf("%d%d", &x, &y);
	    if(x > y){
	        printf(">\n");
	    }
	    else if(x < y){
	        printf("<\n");
	    }
	    else{
	        printf("=\n");
	    }
	}
	return 0;
}