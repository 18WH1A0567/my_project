t = int(input())
for q in range(t):
    s = [str(x) for x in input().split()]
    l = len(s)
    for i in s[:l-1]:
        print(i[0].upper(), end='')
        print('.',end=' ')
    print((s[l-1])[0].upper(), end='')
    print((s[l-1])[1:].lower())

