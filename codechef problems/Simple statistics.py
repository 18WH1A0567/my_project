# cook your dish here

t = int(input())
for i in range(t):
    n, k = [int(x) for x in input().split()]
    l = [int(x) for x in input().split()]
    s = 0
    while(s != k):
        m1 = max(l)
        m2 = min(l)
        l.remove(m1)
        l.remove(m2)
        s += 1
    print(sum(l)/len(l))