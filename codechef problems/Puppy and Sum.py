def sum_(n):
    c = 0
    for i in range(1, n+1):
        c += i
    return c

t = int(input())
for i in range(t):
    n, k = [int(x) for x in input().split()]
    for j in range(n):
        k = sum_(k)
    print(k)