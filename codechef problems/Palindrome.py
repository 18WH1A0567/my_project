def rev(n):
    s = str(n)
    return int( s[::-1])
    
def isPalindrome(n):
    s = str(n)
    if s == s[::-1]:
        return True
    return False

t = int(input()) 
for q in range(t):
    n = int(input())
    c = 0
    while(n != rev(n)):
        n = n + rev(n)
        c += 1
    print(c, n)

