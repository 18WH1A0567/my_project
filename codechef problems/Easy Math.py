# cook your dish here
t = int(input())
for i in range(t):
    n = int(input())
    l = [ int(x) for x in input().split()]
    r = []
    maxi = 1
    for x in range(n-1):
        for y in range(x+1, n):
            maxi = l[x]*l[y];
            s = str(maxi)
            res = 0
            for k in s:
                res += int(k)
            r.append(res)
    print(max(r))
