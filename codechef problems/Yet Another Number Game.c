#include <stdio.h>

int main(void) {
	// your code goes here
	int t, n;
	scanf("%d", &t);
	for(int i = 0;i < t; i++){
	    scanf("%d", &n);
	    if(n % 2 == 0){
	        printf("ALICE\n");
	    }
	    else{
	        printf("BOB\n");
	    }
	}
	return 0;
}

