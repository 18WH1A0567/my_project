t = int(input())
for q in range(t):
    n, k = [int(x) for x in input().split()]
    l = [int(x) for x in input().split()]
    for i in l:
        if i <= k:
            print('1', end = '')
            k -= i
        else:
            print('0', end = '')
    print()

