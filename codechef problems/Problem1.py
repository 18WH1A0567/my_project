def rev(n):
    s = str(n)
    return int( s[::-1])
        
t = int(input()) 
for q in range(t):
    a, b = map(int, input().split())
    a = rev(a)
    b = rev(b)
    print(rev(a+b))