# cook your dish here
t = int(input())
for i in range(t):
    l = [int(x) for x in input().split()]
    print( max(l), sum(l))
