def height(n):
    c = 0
    for i in range(1, n+1):
        c += i
    return c

t = int(input())
for q in range(t):
    n = int(input())
    t = 0
    while(True):
        if height(t) <= n:
            t += 1
        else:
            break
    print(t-1)