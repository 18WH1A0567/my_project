t = int(input())
for q in range(t):
    n = int(input())
    l = [int(x) for x in input().split()]
    r = []
    for i in range(n-1):
        for j in range(i+1, n):
            r.append(l[i]+l[j])
    print(min(r))
