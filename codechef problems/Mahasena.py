# cook your dish here
def isEven(n):
    if n % 2 == 0:
        return True
    return False

n = int(input())
l = [int(x) for x in input().split()]
e = 0
o = 0
for i in l:
    if isEven(i):
        e += 1
    else:
        o += 1
if e > o:
    print('READY FOR BATTLE')
else:
    print('NOT READY')
