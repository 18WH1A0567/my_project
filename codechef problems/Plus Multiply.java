/* package codechef; // don't place package name! */

import java.util.*;
import java.lang.*;
import java.io.*;

/* Name of the class has to be "Main" only if the class is public. */
class Codechef
{
	public static void main (String[] args) throws java.lang.Exception
	{
		// your code goes here
		Scanner scan = new Scanner(System.in);
		int t = scan.nextInt();
		for(int i = 1; i <= t; i++) {
			int n = scan.nextInt(), c = 0;
			int[] a = new int[n];
			
			for(int j = 0; j < n; j++) {
				a[j] = scan.nextInt();
			}
			
			for(int j = 0; j < n-1; j++) {
				for(int k = j+1; k < n; k++) {
					if(a[k] + a[j] == a[k] * a[j]) {
						c++;
					}					
				}
			}
			System.out.println(c);
			
		}
	}
}
