t = int(input())
for q in range(t):
    n = int(input())
    l = [int(x) for x in input().split()]
    lis = []
    for i in range(n):
        if(l[i] == 1):
            lis.append(i)
    
    
    length = len(lis)
    for i in range(length-1):
        if( lis[i+1] - lis[i] < 6):
            print('NO')
            break
    else:
        print('YES')
    