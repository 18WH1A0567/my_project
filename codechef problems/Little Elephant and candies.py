t = int(input())
for i in range(t):
    n, k = map(int, input().split())
    l = list(map(int, input().split()))
    for i in l:
        k -= i
    if k >= 0:
        print('Yes')
    else:
        print('No')
    