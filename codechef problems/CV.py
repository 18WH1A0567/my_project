t = int(input())
for q in range(t):
    n = int(input())
    s = input()
    c = 0
    for i in range(1, n):
        if s[i] in 'aeiou' and s[i-1] not in 'aeiou':
            c += 1
    print(c)