# cook your dish here
t = int(input())
for q in range(t):
    n = int(input())
    l = [int(x) for x in input().split()]
    l.sort()
    l.reverse()
    s = l[0]
    for i in range(1, n):
        if (l[i] - i > 0):
            s += l[i] - i
    print(s % 1000000007)