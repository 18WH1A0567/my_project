# cook your dish here
t = int(input())
for q in range(t):
    x, y, k, n = [int(x) for x in input().split()]
    p = []
    c = []
    f = True
    for i in range(n):
        r1, r2 = [int(x) for x in input().split()]
        p.append(r1)
        c.append(r2)
    for i in range(n):
        if c[i] <= k:
            if y + p[i] >= x:
                print('LuckyChef')
                f = False
                break
    if f :
        print('UnluckyChef')
        
