# cook your dish here
def prefixSum(l, n):
    s = sum(l[0: n+1])    
    return s

def suffixSum(l, k, n):
    s = sum(l[k: n])
    return s


t = int(input())
for q in range(t):
    n = int(input())
    l = [int(x) for x in input().split()]
    r = []
    for i in range(n):
        r.append(prefixSum(l, i) + suffixSum(l, i, n))
    print(r.index(min(r)) + 1)
