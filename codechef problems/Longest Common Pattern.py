t = int(input())
for w in range(t):
    c = 0
    s1 = input()
    temp = set(s1)
    s2 = list(input())
    s2.sort()
    for i in temp:
        if i in s2:
            c += min(s1.count(i), s2.count(i))
    print(c)