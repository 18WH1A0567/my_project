ra = lambda x: map(x, input().strip().split())

for _ in range(int(input())):
    n, k, e, m = ra(int)
    arr = []
    for _ in range(n - 1):
        arr.append(sum(ra(int)))

    serg = sum(ra(int))
    arr = sorted(arr)

    res = max(0, arr[n - k - 1] + 1 - serg)
    if res > m:
        print('Impossible')
    else:
        print(res)