def sumOfDigits(n):
    s = 0
    while(n > 0):
        s += int(n % 10)
        n = int(n/10)
    return s

t = int(input())
for i in range(t):
    x = int(input())
    print(sumOfDigits(x))
