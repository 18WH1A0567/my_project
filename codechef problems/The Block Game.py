n = int(input())
for i in range(n):
    s = input()
    st = "".join(reversed(s))
    if s == st:
        print('wins')
    else:
        print('losses')