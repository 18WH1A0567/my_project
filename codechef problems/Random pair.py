# cook your dish here
def nCr(n): 
    return (fact(n) / (2 * fact(n - 2))) 
  
def fact(n): 
    res = 1
    for i in range(2, n+1): 
        res = res * i           
    return res 


t = int(input())
for q in range(t):
    n = int(input())
    l = [int(x) for x in input().split()]
    r = []
    c = 0
    for i in range(n-1):
        for j in range(i+1, n):
            r.append(l[i] + l[j])
    c = r.count(max(r))
    p = nCr(n)
    print(c/p)



            

