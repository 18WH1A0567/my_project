# cook your dish here
def result(n):
    if n % 10 == 0:
        print('0')
    elif n % 5 == 0:
        print('1')
    else:
        print('-1')


t = int(input())
for i in range(t):
    n = int(input())
    result(n)