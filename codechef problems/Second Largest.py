t = int(input())
for i in range(t):
    l = [int(x) for x in (input().split())]
    x = max(l)
    l.remove(x)
    print(max(l))