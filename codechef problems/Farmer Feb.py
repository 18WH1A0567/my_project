# cook your dish here
def isPrime(n):
    for i in range(2, n//2 + 1):
        if n % i == 0:
            return False
    return True

t = int(input())
for i in range(t):
    x, y = [int(x) for x in input().split()]
    x = x + y
    i = 1
    while(True):
        if isPrime(x+i):
            print(i)
            break
        i += 1


