t = int(input())
for i in range(t):
    s = input()
    c = 0
    for i in s:
        if i in 'ADOQPR':
            c += 1
        elif i == 'B':
            c += 2
    print(c)
    