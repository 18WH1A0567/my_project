t=int(input())
for i in range(t):
    n=input()
    c=0
    m=n[0]
    for j in range(1,len(n)):
        if m!=n[j]:
            c+=1
            m=n[j]
        if c>2:
            print("non-uniform")
            break
    if c<=2:
        print("uniform")