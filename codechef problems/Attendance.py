# cook your dish here
t = int(input())
for q in range(t):
    n = int(input())
    f = []
    l = []
    for i in range(n):
        s, p = (str(x) for x in input().split())
        f.append(s)
        l.append(p)
    for i in range(n):
        if f.count(f[i]) == 1:
            print(f[i])
        else:
            print(f[i], l[i])
    