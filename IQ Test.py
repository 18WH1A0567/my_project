def isEven(n):
    if n % 2 == 0:
        return True
    return False
 
n = int(input())
l = [int(x) for x in input().split()]
k = 0
m = 0
o = 0
e = 0
for i in range(n):
    if isEven(l[i]):
        e += 1
        k = i
    else:
        o += 1
        m = i
if e == 1:
    print(k+1)
else:
    print(m+1)
 