
n = int(input())
a = [int(x) for x in input()]
b = [int(x) for x in input()]
c = 0
for i in range(n):
    x = abs(a[i] - b[i])
    if x < 10 - x:
        c += x
    else:
        c += (10 - x)
print(c)
