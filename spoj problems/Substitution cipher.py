n = int(input())
m = input()
t = int(input())
for q in range(t):
    f = input()
    for i in f:
        if i in m:
            if m.index(i) == n-1:
                print(m[0], end = '')
            else:
                print(m[m.index(i) + 1], end = '')
        else:
            print(i, end = '')
    print()