a, b = [int(x) for x in input().split()]
s = 0
for i in range(a, b+1):
    s += pow(i, 2)
print(s)

