#include<stdio.h>

int main(){
        int  t, r, n, m , c, i, j, f, s;
        scanf("%d", &t);

        for(int q = 0; q < t; q++){
                scanf("%d%d%d%d", &n, &m, &r, &c);
                f = c+1;
                s = c-1;
                for(i = r; i >= 1; i--){
                        f--;
                        s++;
                }
                for(i = 1; i <= n; i++){
                        for(j = 1; j <= m; j++){
                                if(j == f || j == s){
                                        printf("*");
                                }
                                else{
                                        printf(".");
                                }
                        }
                        printf("\n");
                        f++;
                        s--;
                }
        }
        return 0;
}

                
                