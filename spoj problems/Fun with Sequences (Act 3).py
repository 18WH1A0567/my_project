n = int(input())
s = [int(x) for x in input().split()]
m = int(input())
q = [int(x) for x in input().split()]
for i in range(min(n, m)):
    if s[i] == q[i]:
        print(i+1, end = ' ')
print()        