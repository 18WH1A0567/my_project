t = int(input())
for q in range(t):
    x, y = [int(x) for x in input().split()]
    if x % 2 == 0 and (x == y + 2 or x == y):
        print(x + y)
    elif x % 2 != 0 and (x == y or x == y + 2):
        print(x+y-1)
    else:
        print('No Number')
