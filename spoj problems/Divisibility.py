t = int(input())
for i in range(t):
    n, x, y = [int(x) for x in input().split()]
    for j in range(1, n):
        if j % x == 0 and j % y != 0:
            print(j, end=' ')
    print()
