t = int(input())
for q in range(t):
    n = int(input())
    b = [int(x) for x in input().split()]
    g = [int(x) for x in input().split()]
    s = 0
    b.sort()
    g.sort()
    for i in range(n):
        s += (b[i] * g[i])
    print(s)