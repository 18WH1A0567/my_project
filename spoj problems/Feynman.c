#include <stdio.h>

void count(int n){
	int s = ( ( n * ( n+1 ) * ( 2*n+1)) / 6);
	printf("%d\n", s);
}

int main(void) {
	// your code here
	int n;
	while(1){
		scanf("%d", &n);
		if(n == 0){
			break;
		}
		count(n);
	}

	return 0;
}