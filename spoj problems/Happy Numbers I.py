def sum_digits(n):
    s = 0
    while(n > 0):
        r = n % 10
        s += pow(r, 2)
        n //= 10
    return s

n = int(input())
l = []
l.append(n)
c = 1
while(True):
    n = sum_digits(n)
    if n == 1:
        print(c)
        break
    if n in l:
        print('-1')
        break
    l.append(n)
    c += 1




