#include<stdio.h>

void print_first(int n){
        printf("*");
        for(int i = 1; i <= n; i++){
                printf("***");
        }
        //printf("\n");
}

int main(){
        int  t, n, m , i, j;
        scanf("%d", &t);

        for(int q = 0; q < t; q++){
                scanf("%d%d", &n, &m);
                
                //print_first(n);
                for(i = 0; i <= n*3; i++){
                        if(i % 3 == 0){
                                print_first(m);
                        }
                        else{
                                printf("*");
                                for(j = 1; j <= m; j++){
                                        printf("..*");
                                }
                        }
                        printf("\n");
                }
        }
        return 0;
}

                
                