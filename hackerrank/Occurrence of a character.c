#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>

int main() {

    /* Enter your code here. Read input from STDIN. Print output to STDOUT */
    char s[5000];
    scanf("%[^\n]%*c", s);
    char ch ;
    scanf("%c", &ch);
    int i = 0, c = 0;
    while(s[i] != '\0'){
        if (s[i] == ch){
            c++;
        }
        ++i;
    }
    printf("%d", c);
    return 0;
}
