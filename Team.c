#include<stdio.h>
 
int main(){
        int n, i, j, f = 0, c ;
        scanf("%d", &n);
        int a[n][3];
        for(i = 0; i < n; i++){
                for(j = 0; j < 3; j++){
                        scanf("%d", &a[i][j]);
                }
        }
        for(i = 0; i < n; i++){
                c = 0;
                for(j = 0; j < 3; j++){
                        if(a[i][j] == 1){
                                c++;
                        }
                }
                if(c >= 2){
                        f++;
                }
        }
        printf("%d\n", f);
        return 0;
}
 